import urllib.request
import re

quote_page = 'http://192.168.0.150/digital.cgi?chgrp='

try:
    page = urllib.request.urlopen(quote_page)
except:
    print ("Something wrong")

mybyte = page.read()

# Decode in html code
html = mybyte.decode("utf8")
page.close()

regex_pattern = r'[-+] \d*\.\d*'

# Return a tuple of matches
match = re.findall(regex_pattern, html)

print (match)
