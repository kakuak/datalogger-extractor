# coding: utf8

import urllib.request
import re
import time
import datetime
import csv
import sys
import pandas as pd
import os.path

quote_page = 'http://192.168.0.150/digital.cgi?chgrp='

# Check if URL ok
def check_url(quote_page):
    # quote_page = 'http://192.168.0.150/digital.cgi?chgrp='
    try:
        page = urllib.request.urlopen(quote_page)
    except Exception as e:
        print(str(e))
        print('/!\ Website DOWN /!\\')
        sys.exit(1)

# Return html code
def get_html():
    quote_page = 'http://192.168.0.150/digital.cgi?chgrp='

    try:
        page = urllib.request.urlopen(quote_page)
        mybyte = page.read()

        # Decode in html code
        html = mybyte.decode("utf8")
        page.close()
        return html
    except Exception as e:
        print(str(e))
        sys.exit(1)

def get_data():
    html = get_html()
    # Regex pattern to match float
    regex_pattern = r'[-+][ ]*\d+\.\d*'
    # Compile regex pattern
    prog = re.compile(regex_pattern)
    # Find all floating
    result = prog.findall(html)
    # Remove space between signe & floating
    data_csv = [x.replace(' ', '') for x in result]
    return data_csv

# Arg: array of values
# Save data in csv file
def updateCsv(data_csv):
    # Open csv file
    with open('dataLogger.csv', 'a', newline='') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        # Add Date&Time
        time_now = datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        data_csv.insert(0,time_now)

        # Append data_csv in file
        filewriter.writerow(data_csv)
        print (",".join(data_csv))
        csvfile.close()

# Write Headers in csv file
def get_headers(quote_page):
    # If file already exists then exit funct
    file_name = datetime.datetime.now().strftime('%Y%m%d') + '.csv'
    if os.path.isfile(file_name):
        print ('[*]', file_name, 'already exists\n[*] Will append to this file')
        return

    tables = pd.read_html(quote_page)

    # Array of channel name
    first_header = ['Date&Time']
    # Array of unit of measurement
    second_header = ['Time']
    # Array of value
    values = []
    # Extract html table
    for k in range(0, 4):
        for i in range(0, 5):
            val = tables[0][0][2 + 16 * k + i * 3]
            if val == 'Off':
                break
            # Retrieve channel name
            first_header.append(tables[0][0][1 + 16 * k + i * 3].replace(' ', ''))
            # Retrieve unit of measurement
            second_header.append(tables[0][0][3 + 16 * k + i * 3].replace(' ',''))
            values.append(val.replace(' ',''))

    # Create new csv file
    with open(file_name, 'w', newline='') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        # Write headers in file
        # 1st line in csv file => Date&Time,CH1,CH2,...,CH20
        filewriter.writerow(first_header)
        # 2nd line in csv file => Time,degC,kgf/cm2,mV,...,V
        filewriter.writerow(second_header)
        csvfile.close()
    updateCsv(values)

# Launch record with pause
def my_record():
    seconds = float("Enter Seconds: ")
    starttime = time.time()
    while True:
        time.sleep(seconds - ((time.time() - starttime) % seconds))
        data = get_data()
        updateCsv(data)

if __name__ == "__main__":
    #s = int(input("Enter secondes: "))
    #get_headers()
    #my_record(s)
    check_url()
